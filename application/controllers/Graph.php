<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Graph extends CI_Controller{
	public function __construct(){
		parent::__construct();
        if(!$this->session->userdata('username')){redirect('login');}
		$this->load->model(array('user_model'));	
        $this->load->library(array('form_validation'));    
		
	}


    public function show(){
        date_default_timezone_set ('UTC');
        $this->form_validation->set_rules('market','Market','trim|required');
        $this->form_validation->set_rules('instruments','instruments','trim|required');
        $this->form_validation->set_rules('time','time','trim|required'); 
 
        if($this->form_validation->run()===FALSE) { 
            $this->load->view('partials/header');
            $this->load->view('graph/show',array());
            $this->load->view('partials/footer');
          
        
        }else  {   
            $market = $this->input->post('market');
            $instruments = $this->input->post('instruments');
            $time = $this->input->post('time');

            $exchange = '\\ccxt\\'.$market;
            $exchange = new $exchange (array (
                'enableRateLimit' => true          
            ));    

            if ($exchange->has['fetchOHLCV']){
                usleep ($exchange->rateLimit * 1000);
                $ohlcv = $exchange->fetch_ohlcv ($instruments, $time);
                echo json_encode($ohlcv);
                    
            }           


     
        }
      
    }

    public function view(){
           $this->load->view('partials/header');
            $this->load->view('graph/show',array());
            $this->load->view('partials/footer');
    }
 


	











}


