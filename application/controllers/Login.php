<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model(array('user_model'));	
		
	}

    public function index(){	
		$this->load->view('login', array());
	
    }

    public function c(){
        var_dump($this->session->userdata('member_id'));
        



    }
   public function  britrex(){
        // $poloniex = new \ccxt\poloniex  ();
        $bittrex  = new \ccxt\bittrex   (array ('verbose' => true));
        // $poloniex_markets = $poloniex->load_markets ();

        //var_dump ($poloniex_markets);
        // var_dump ($bittrex->load_markets ());   

        // var_dump ($poloniex->fetch_order_book ($poloniex->symbols[0]));
        var_dump($bittrex->fetch_trades ('ETH/BTC'));
       
    


   }


    public function test(){
        date_default_timezone_set ('UTC');
        $exchange = '\\ccxt\\bitfinex2';

        $exchange = new $exchange (array (
            'enableRateLimit' => true              
        ));   

        if ($exchange->has['fetchOHLCV']){
            usleep ($exchange->rateLimit * 1000);
            $ohlcv = $exchange->fetch_ohlcv ('BTC/USD', '30m');
            echo json_encode($ohlcv);
               
        }  


      
    }




    public function testview(){
           $this->load->view('partials/header');
            $this->load->view('dashboard/chart',array());
            $this->load->view('partials/footer');
    }
 


	public function login(){
		$username = $this->input->post('username');
		$password =  md5($this->input->post('password'));
		if($this->user_model->login($username,$password)){
			redirect('graph/view');
		}else{
			$this->session->set_flashdata('message','Incorrect Credentials');
			redirect('login/index');
		}


    }

    public function show(){
    	if(!$this->session->userdata('username')){redirect('login');}

    	$this->load->view('header');    

	    $this->load->view('show',array(
	        'data'=>$this->user_model->get()
	    ));
	    $this->load->view('footer');

    }

    public function register(){
        $this->form_validation->set_rules('username','Username','trim|required|callback_check_username');     
        $this->form_validation->set_rules('password','Password','trim|min_length[4]|max_length[20]|required');
        $this->form_validation->set_rules('confirm_password','Confirm password','trim|matches[password]|required');

        $this->form_validation->set_rules('phone','Phone','trim|required');
        $this->form_validation->set_rules('email','E-mail','trim|required');
        $this->form_validation->set_rules('first_name','First Name','trim|required');
        $this->form_validation->set_rules('last_name','Last Name','trim|required');
 
 
        if($this->form_validation->run()===FALSE) {                    
            $this->load->view('register',array());
           
        }else  {           
            $password = md5($this->input->post('password'));
            $this->user_model->register($password);
            $this->session->set_flashdata(array('notice'=>'Registration Successful'));
            redirect('login');
           


     
        }
    }




     
    public function check_username($username){
        if($this->user_model->check_username($username)){
            $this->form_validation->set_message('check_username', '{field}  Already Exsit');
             return FALSE;
        }else{
            return True;
        }
    }



    public function logout(){
    	$this->session->sess_destroy();
    	redirect('login/index');


    }











}


