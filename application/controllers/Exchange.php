<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Exchange extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		if(!$this->session->userdata('username')){redirect('login');}	
		$this->load->model(array('exchange_model'));
		$this->load->library("pagination");

	}


	public function index(){	
	

		$this->load->view('header',array('page'=>'exchange'));
		$this->load->view('exchange/show',array('data'=>$this->exchange_model->get()));
		$this->load->view('footer');
	}
	


	public function add(){	
		$this->form_validation->set_rules('name', 'Name', "trim|required");
    	$this->form_validation->set_rules('apikey', 'API Key', "trim|required");
    	$this->form_validation->set_rules('apisecret', 'API Secret', "trim|required");
 	

        if ($this->form_validation->run() == FALSE){  
        	$this->load->view('header');
			$this->load->view('exchange/add',array(	
				 	'data' => $this->exchange_model->get()		
			));
			$this->load->view('footer');
        }else{
        	$this->exchange_model->save();
        	$this->session->set_flashdata(array('notice'=>'API has being Save'));
        	redirect('exchange/index');
        	    
        }


	}


	public function add_bot(){	
		$this->form_validation->set_rules('name', 'name', "trim|required");
    	$this->form_validation->set_rules('sell_limit', 'sell_limit', "trim|required");
    	$this->form_validation->set_rules('buy_limit', 'buy_limit', "trim|required");

        if ($this->form_validation->run() == FALSE){  
        	$this->load->view('header');
			$this->load->view('exchange/bot',array(	
				 	'data' => $this->exchange_model->get()		
			));
			$this->load->view('footer');
        }else{
        	$this->exchange_model->save_bot();
        	$this->session->set_flashdata(array('notice'=>'Bot has being Activated'));
        	redirect('dashboard/britrex');
        	    
        }


	}



	

	











	







}
