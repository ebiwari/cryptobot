<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> <!-- /widget -->
<h4><?= $this->session->flashdata('notice'); ?></h4>
<p><a class="btn btn-success" href="<?= site_url('login/register') ?>">Add Employee</a></p>
<div class="widget widget-table action-table">

<div class="widget-header"> <i class="icon-th-list"></i>


<h3>All Registered Employee</h3>
</div>
<!-- /widget-header -->
<div class="widget-content">
<table class="table table-striped table-bordered">
<thead>
<tr>

<th>First Name </th>
<th>Last Name</th>
<th>Phone </th>



<th class="td-actions"> </th>
</tr>
</thead>
<tbody>

<?php if(isset($data) && $data != Null): ?>
<?php foreach($data as $row): ?>
<tr>

<td><?= strtoupper($row->first_name); ?></td>
<td><?= strtoupper($row->last_name); ?></td>


<td><?= $row->phone; ?></td>


<td class="td-actions"><a href='#' class="btn btn-small btn-success"><i class="btn-icon-only icon-ok"> </i></a><a href='#' class="btn btn-danger btn-small"><i class="btn-icon-only icon-remove"> </i></a></td>
</tr>

<?php endforeach; endif; ?>


</tbody>
</table>

</div>
<!-- /widget-content --> 
</div>
<!-- /widget --> 



















</div> <!-- /span8 -->
</div> <!-- /row -->
</div> <!-- /container -->
</div> <!-- /main-inner -->
</div> <!-- /main -->