<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Login -BOT Management System</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes"> 
<link href="<?= base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url(); ?>public/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url(); ?>public/css/font-awesome.css" rel="stylesheet">
<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet"> -->
<link href="<?= base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>public/css/pages/signin.css" rel="stylesheet" type="text/css">
</head>

<body>

<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</a>
<a class="brand" href="<?= site_url('login'); ?>">BOT SYSTEM</a>
<div class="nav-collapse">
<ul class="nav pull-right">
<li class="">						
	<a href="<?= site_url('login/register'); ?>" class="">Don't have an account?	</a>	
</li>
<li class="">						
	<a href="<?= site_url('login'); ?>" class=""><i class="icon-chevron-left"></i>Back to Homepage</a>	
</li>
</ul>
</div><!--/.nav-collapse -->
</div> <!-- /container -->
</div> <!-- /navbar-inner -->
</div> <!-- /navbar -->



<div class="account-container">
<div class="content clearfix">
<?= form_open("login/register"); ?>

<h1>Member Registration</h1>
<div class="login-fields">
<p>Please provide your details</p>

<?= validation_errors(); ?>



<div class="field <?php if(form_error('username')){echo 'warning'; } ?>">
	<label for="username">Username</label>
	<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" value="<?= set_value('username'); ?>" />
</div> <!-- /field -->

<div class="field <?php if(form_error('email')){echo 'warning'; } ?>">
	<label for="email">E-mail</label>
	<input type="email" id="email" name="email" value="" placeholder="E-mail" class="login username-field" 
	value="<?= set_value('email'); ?>" />
</div> <!-- /field -->


<div class="field">
	<label for="password">Password:</label>
	<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
</div> <!-- /password -->


<div class="field">
	<label for="cpassword">Confirn Password:</label>
	<input type="password" id="confirm_password" name="confirm_password" value="" placeholder="Confirm Password" class="login password-field"/>
</div> <!-- /password -->


<div class="field <?php if(form_error('phone')){echo 'warning'; } ?>">
	<label for="phone">Phone:</label>
	<input type="text" id="password" name="phone" value="" placeholder="Phone" class="login username-field" 
	value="<?= set_value('phone'); ?>"
	/>
</div> <!-- /password -->

<div class="field <?php if(form_error('first_name')){echo 'warning'; } ?>">
	<label for="password">First Name</label>
	<input type="text" id="password" name="first_name" value="" placeholder="First Name" class="login username-field" value="<?= set_value('first_name'); ?>" />
</div> <!-- /password -->

<div class="field <?php if(form_error('last_name')){echo 'warning'; } ?>">
	<label for="password">Last Name</label>
	<input type="text" id="password" name="last_name" value="" placeholder="Last Name" class="login username-field"
	 value="<?= set_value('last_name'); ?>" />
</div> <!-- /password -->



</div> <!-- /login-fields -->

<div class="login-actions">
<span class="login-checkbox">

</span>
					
<button class="button btn btn-success btn-large" type="submit">Register</button>
</div> <!-- .actions -->

</form>

</div> <!-- /content -->
</div> <!-- /account-container -->





<script src="<?= base_url(); ?>public/js/jquery-1.7.2.min.js"></script>
<script src="<?= base_url(); ?>public/js/bootstrap.js"></script>

<script src="<?= base_url(); ?>public/js/signin.js"></script>

</body>

</html>
