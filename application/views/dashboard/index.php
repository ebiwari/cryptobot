<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span6">
<div class="widget widget-nopad">
<div class="widget-header"> <i class="icon-list-alt"></i><h3> Today's Stats</h3></div>

<!-- /widget-header -->
<div class="widget-content">
<div class="widget big-stats-container">
<div class="widget-content">

<p style="text-align: center;"><a href="<?= site_url('dashboard/britrex'); ?>"><img src="<?= base_url(); ?>images/britrex.jpg" class="img-rounded" ></a></p>

<p style="text-align: center;"><img src="<?= base_url(); ?>images/britrex.jpg" class="img-rounded" ></p>
<p style="text-align: center;"><img src="<?= base_url(); ?>images/britrex.jpg" class="img-rounded" ></p>
<!-- /widget-content --> 

</div>
</div>
</div>
<!-- /widget -->


<!-- /widget --> 
</div>
</div>
<!-- /span6 -->
<div class="span6">
<div class="widget">
<div class="widget-header"> <i class="icon-bookmark"></i>
<h3>Important Shortcuts</h3>
</div>
<!-- /widget-header -->
<div class="widget-content">
<div class="shortcuts"> 
<a href="<?= site_url('dashboard/index'); ?>" class="shortcut">
<i class="shortcut-icon icon-list-alt"></i><span
class="shortcut-label">Dashboard</span> 
</a><a href="<?= site_url('dashboard/add'); ?>" class="shortcut"><i
class="shortcut-icon icon-bookmark"></i><span class="shortcut-label">Market</span> </a><a href="<?= site_url('report/index'); ?>" class="shortcut"><i class="shortcut-icon icon-signal"></i> <span class="shortcut-label"></span> </a><a href="javascript:;" class="shortcut"> <i class="shortcut-icon icon-comment"></i><span class="shortcut-label">Accounts</span> </a><a href="javascript:;" class="shortcut"><i class="shortcut-icon icon-user"></i><span
class="shortcut-label">Funds</span> </a><a href="<?= site_url('cart/index'); ?>" class="shortcut"><i
class="shortcut-icon icon-file"></i><span class="shortcut-label">Cart</span> </a><a href="<?= site_url('dashboard/add_doctor'); ?>" class="shortcut"><i class="shortcut-icon icon-picture"></i> <span class="shortcut-label">Public</span> </a><a href="javascript:;" class="shortcut"> <i class="shortcut-icon icon-tag"></i><span class="shortcut-label">Trade</span> </a> </div>
<!-- /shortcuts --> 
</div>
<!-- /widget-content --> 
</div>
<!-- /widget -->
<div class="widget">
<div class="widget-header"> <i class="icon-signal"></i>
<h3> Area Chart Example</h3>
</div>
<!-- /widget-header -->
<div class="widget-content">
<canvas id="area-chart" class="chart-holder" height="250" width="538"> </canvas>
<!-- /area-chart --> 
</div>
<!-- /widget-content --> 
</div>
<!-- /widget -->

<!-- /widget --> 

<!-- /span6 --> 
</div>
<!-- /row --> 
</div>
<!-- /container --> 
</div>
<!-- /main-inner --> 
</div>
<!-- /main -->
<div class="extra">
<div class="extra-inner">
<div class="container">
<div class="row">
<div class="span3">
    <h4>
        About Scanex</h4>
    <ul>
        <li><a href="javascript:;">Consultancy</a></li>
        <li><a href="javascript:;">Imaging</a></li>
        <li><a href="javascript:;">Radiation</a></li>
        <li><a href="javascript:;">Clocking Time</a></li>
    </ul>
</div>
<!-- /span3 -->
<div class="span3">
    <h4>
        About Scanex</h4>
    <ul>
        <li><a href="javascript:;">Consultancy</a></li>
        <li><a href="javascript:;">Imaging</a></li>
        <li><a href="javascript:;">Radiation</a></li>
        <li><a href="javascript:;">Clocking Time</a></li>
    </ul>
</div>
<!-- /span3 -->
<div class="span3">
    <h4>
        Scannex Legal</h4>
    <ul>
        <li><a href="javascript:;">Read License</a></li>
        <li><a href="javascript:;">Terms of Use</a></li>
        <li><a href="javascript:;">Privacy Policy</a></li>
    </ul>
</div>
<!-- /span3 -->
<div class="span3">
     <h4>
        About Scanex</h4>
    <ul>
        <li><a href="javascript:;">Consultancy</a></li>
        <li><a href="javascript:;">Imaging</a></li>
        <li><a href="javascript:;">Radiation</a></li>
        <li><a href="javascript:;">Clocking Time</a></li>
    </ul>
</div>
<!-- /span3 -->
</div>
<!-- /row --> 
</div>
<!-- /container --> 
</div>
<!-- /extra-inner --> 
</div>
