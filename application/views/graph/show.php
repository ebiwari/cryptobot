
<div class="main-inner">
<div class="container-fluid">

<div class="row-fluid" style="margin-top: -10px;">
<div class="span12">




<table>
<?= form_open('graph/view'); ?>

<tr  style="text-align: left">
<th>Markets</th>
<th>Instrumenst</th>
<th>Time</th>	
</tr>


<tr>
<td>
<select name="market" id="market" required>

  <option></option>


  <option value="poloniex">Poloniex</option>
  <option value="bitfinex">Bitfinex</option>
  <option value="bittrex">Bittrex</option>
  <option value="binance">Binance</option>
  <option value="bitstamp">Bitstamp</option>

  <option value="gdax">GDAX</option>
  <option value="huobipro">Huobipro</option>
  <option value="wex.nz">wex.nz</option>

  <option value="cex.io">cex.io</option>
  <option value="quoine">Quoine</option>
  <option value="cryptsy">Cryptsy</option>
</select>
</td>

<td>

<select name="instruments" id="instruments" required>

  <option></option>

   <option value="ETH/BTC"  data-chained="poloniex">ETH/BTC</option>
   <option value="BCH/BTC"  data-chained="poloniex">BCH/BTC</option>
   <option value="BCH/USDT"  data-chained="poloniex">ZEC/BTC</option>

   <option value="DASH/BTC"  data-chained="poloniex">DASH/BTC</option>
   <option value="XRP/BTC"  data-chained="poloniex">XRP/BTC</option>
   <option value="SDC/BTC"  data-chained="poloniex">SDC/BTC</option>
   <option value="GNT/BTC"  data-chained="poloniex">GNT/BTC</option>

   <option value="BTC/USD"  data-chained="bitfinex">BTC/USD</option>
   <option value="BCH/USD"  data-chained="bitfinex">BCH/USD</option>
   <option value="BCH/BTC"  data-chained="bitfinex">BCH/BTC</option>

   <option value="ETH/USD"  data-chained="bitfinex">ETH/USD</option>
   <option value="ETH/BTC"  data-chained="bitfinex">ETH/BTC</option>


   <option value="ETH/BTC"  data-chained="bittrex">ETH/BTC</option>
   <option value="BCC/BTC"  data-chained="bittrex">BCC/BTC</option>
   <option value="NEO/BTC"  data-chained="bittrex">NEO/BTC</option>

   <option value="LTC/BTC"  data-chained="bittrex">LTC/BTC</option>
   <option value="DGD/BTC"  data-chained="bittrex">DGD/BTC</option>



</select>
</td>	

<td> 
<select name="time"  class = 'input-mini'>
  
  <option></option>
  <option value="1m">1m</option>
  <option value="15m">15m</option>
  <option value="1h">1h</option>
  <option value="2h">2h</option>
  <option value="8h">8h</option>

  <option value="1d">1d</option>

</select>
</td>

<td> <button type="submit" class="btn" >Get Markets</button></td>
</tr>


</form>
	

</table>





	

</div>
</div>
<div class="row-fluid">
<div class="span12"> 
<!-- /widget -->
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<!-- /widget --> 

</div> <!-- /span8 -->
</div> <!-- /row -->
</div> <!-- /container -->
</div> <!-- /main-inner -->
</div> <!-- /main -->