<!-- //w3_agileits_top_nav-->
<!-- /inner_content-->
<div class="inner_content">
<!-- /inner_content_w3_agile_info-->
<div class="inner_content_w3_agile_info">
<!-- /agile_top_w3_grids-->


<div class="forms-main_agileits">
<div class="graph-form agile_info_shadow">

<h3 class="w3_inner_tittle two">Send Message</h3>

<div class="form-body">
<?= form_open('tickets/message'); ?>



<div class="clearfix"></div> 


<div style='color:red'><?= validation_errors(); ?></div>

<?php if(isset($student_data)): ?>
<div class="form-group <?php if(form_error('type')){echo 'has-error'; } ?>"> 
<label for="student_id" class="col-sm-2 control-label">Student Name</label>
<select class="selectpicker" data-live-search="true" data-width="75%" name="student_id">
<option>Click and Filter</option>
<?php foreach ($student_data as $student): ?>
	<option value="<?= $student->id; ?>">
	<?= strtoupper($student->first_name).' '.strtoupper($student->last_name).' '.strtoupper($student->other_name); ?>	
	</option>
<?php endforeach; ?>

</select>
</div> 
<?php endif; ?>

<div style="margin-bottom:20px" class="form-group <?php if(form_error('title')){echo 'has-error'; } ?>" id='travel_routes'> 
<label for="travel_mode" class="col-sm-2 control-label">Title</label>
<input type='text' name="title" class="form-control" ></textarea>
</div>



<div style="margin-bottom:20px" class="form-group <?php if(form_error('message')){echo 'has-error'; } ?>" id='travel_routes'> 
<label for="travel_mode" class="col-sm-2 control-label">Message</label>
<textarea name="message" class="form-control" rows="3"></textarea>
</div>





<button type="submit" class="btn btn-default">Send Message</button> 



 </form> 

 <div class="agile-tables">
<div class="w3l-table-info agile_info_shadow">
<h3 class="w3_inner_tittle two">Inbox Message</h3>
<table id="table">
<thead>
  <tr>
	<th>Ticket Id</th>
	<th>Title</th>
	<th>Message</th>


	<th>Created At</th>

	
  </tr>
</thead>
<?php if(isset($data) && $data != Null): ?>
<tbody>
<?php foreach($data as $row ): ?>
<td><?= $row->id; ?></td>



<td><?= substr($row->message,0,10); ?></td>


<td><?= $row->created_at; ?></td>



</tr>
<?php endforeach; ?>

</tbody>
<?php endif; ?>
</table>

</div>
</div>

</div>

</div>



<div class="clearfix"></div>





				

<!-- //inner_content_w3_agile_info-->
</div>
<!-- //inner_content-->
</div>