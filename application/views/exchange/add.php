<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 
<div class="widget ">
<div class="widget-header">
<i class="icon-user"></i>
<h3>Add API Info</h3>
</div> <!-- /widget-header -->

<div class="widget-content">



<?php  $attributes = array('class' => 'form-horizontal', 'id' => 'myform'); ?>
<?= form_open('exchange/add', $attributes); ?>
<fieldset>
<div class="control-group  <?php if(form_error('name')){echo 'warning'; } ?>">											
<label class="control-label" for="name">NAME</label>
<div class="controls">
	<input type="text" class="span6" name="name" id="name" value="<?= set_value('name'); ?>">
</div> <!-- /controls -->				
</div> <!-- /control-group -->


<div class="control-group <?php if(form_error('apikey')){echo 'warning'; } ?>">											
<label class="control-label" for="apikey">API KEY</label>
<div class="controls">
	<input type="text" class="span6" name = 'apikey' id="apikey"  value="<?= set_value('apikey'); ?>">
</div> <!-- /controls -->				
</div> <!-- /control-group -->


<div class="control-group <?php if(form_error('apisecret')){echo 'warning'; } ?>">											
<label class="control-label" for="apisecret">API SECRET</label>
<div class="controls">
	<input type="text" class="span6" name = 'apisecret' id="apisecret"  value="<?= set_value('apisecret'); ?>">
</div> <!-- /controls -->				
</div> <!-- /control-group -->














<br />


<div class="form-actions">
<button type="submit" class="btn btn-primary">Save</button> 
<button class="btn">Cancel</button>
</div> <!-- /form-actions -->
</fieldset>
</form>
</div>








</div> <!-- /widget-content -->
</div> <!-- /widget -->
</div> <!-- /span8 -->
</div> <!-- /row -->
</div> <!-- /container -->
</div> <!-- /main-inner -->
</div> <!-- /main -->
