<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 
<div class="widget ">
<div class="widget-header">
<i class="icon-user"></i>
<h3>Add Doctor</h3>
</div> <!-- /widget-header -->

<div class="widget-content">


<?php if(isset($data) && $data != Null): ?>

<?php  $attributes = array('class' => 'form-horizontal', 'id' => 'myform'); ?>
<?= form_open('dashboard/edit', $attributes); ?>
<fieldset>
<div class="control-group  <?php if(form_error('first_name')){echo 'warning'; } ?>">                      
<label class="control-label" for="first_name">First Name</label>
<div class="controls">
  <input type="hidden" name="id" value="<?= $data->id; ?>">
  <input type="text" class="span6" name="first_name" id="first_name" value="<?= $data->first_name != ''?$data->first_name:set_value('first_name'); ?>">
</div> <!-- /controls -->       
</div> <!-- /control-group -->


<div class="control-group <?php if(form_error('last_name')){echo 'warning'; } ?>">                      
<label class="control-label" for="last_name">Last Name</label>
<div class="controls">
  <input type="text" class="span6" name = 'last_name' id="last_name" value="<?= $data->last_name != ''?$data->last_name:set_value('last_name'); ?>">
</div> <!-- /controls -->       
</div> <!-- /control-group -->



<div class="control-group">                     
<label class="control-label" for="other_name">Other Name</label>
<div class="controls">
  <input type="text" class="span6" name = 'other_name' id="other_name" value="<?= $data->other_name != ''?$data->other_name:''; ?>">
</div> <!-- /controls -->       
</div> <!-- /control-group -->


>



<div class="control-group  <?php if(form_error('phone')){echo 'warning'; } ?>">                     
<label class="control-label" for="phone">Phone</label>
<div class="controls">
  <input type="text" class="span4" name="phone" id="phone" value="<?= $data->phone != ''?$data->phone:set_value('phone'); ?>">
</div> <!-- /controls -->       
</div> <!-- /control-group -->


<br /><br />


<div class="control-group  <?php if(form_error('gender')){echo 'warning'; } ?>">                      
<label class="control-label">Gender</label>
<div class="controls">

<?php if(strtolower(trim($data->gender)) === 'male'): ?>

  <label class="radio inline">
    <input type="radio"  name="gender" value="male" checked> Male
  </label>

  <label class="radio inline">
    <input type="radio" name="gender" value="female">Female
  </label>

<?php else: ?>

  <label class="radio inline">
    <input type="radio"  name="gender" value="male" > Male
  </label>

  <label class="radio inline">
    <input type="radio" name="gender" value="female"  checked>Female
  </label>

<?php endif; ?>




</div>  <!-- /controls -->      
</div> <!-- /control-group -->






<br />


<div class="form-actions">
<button type="submit" class="btn btn-primary">Save</button> 
<button class="btn">Cancel</button>
</div> <!-- /form-actions -->
</fieldset>
</form>



<?php endif; ?>
</div>








</div> <!-- /widget-content -->
</div> <!-- /widget -->
</div> <!-- /span8 -->
</div> <!-- /row -->
</div> <!-- /container -->
</div> <!-- /main-inner -->
</div> <!-- /main -->