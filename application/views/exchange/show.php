<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 


<!-- /widget -->

<h4><?= $this->session->flashdata('notice'); ?></h4>
<p><a class="btn btn-success" href="<?= site_url('exchange/add') ?>">Add API</a></p>
<div class="widget widget-table action-table">

<div class="widget-header"> <i class="icon-th-list"></i>




<h3>All API Setting</h3>
</div>
<!-- /widget-header -->
<div class="widget-content">
<table class="table table-striped table-bordered">
<thead>
<tr>
<th> Name </th>
<th> API Keys</th>
<th> API Secrets</th>


<th class="td-actions"> </th>
</tr>
</thead>
<tbody>



<?php if(isset($data) && $data != Null): ?>

<tr>
<td><?= strtoupper($data->name); ?></td>
<td><?= $data->apikey; ?></td>
<td><?= $data->apisecret; ?></td>

<td class="td-actions"><a href='<?= site_url("dashboard/view/$row->id"); ?>' class="btn btn-small btn-success"><i class="btn-icon-only icon-ok"> </i></a><a href='#' class="btn btn-danger btn-small"><i class="btn-icon-only icon-remove"> </i></a></td>
</tr>

<?php endif; ?>


</tbody>
</table>


</div>
<!-- /widget-content --> 
</div>
<!-- /widget --> 



















</div> <!-- /span8 -->
</div> <!-- /row -->
</div> <!-- /container -->
</div> <!-- /main-inner -->
</div> <!-- /main -->