<div class="main">
<div class="main-inner">
<div class="container">


<div class="row">
<div class="span8">
<!-- /widget -->
<div class="widget widget-table action-table">
<div class="widget-header"> <i class="icon-th-list"></i>
<h3>BRITTEX EXCHANGE</h3>
</div>
<!-- /widget-header -->
<div class="widget-content">
<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Market Name</th>
<th>High</th>
<th>Low</th>
<th> TimeStamp</th>
</tr>
</thead>
<tbody>
<?php if(isset($data) && $data != Null): ?>
<?php foreach($data->result as $row): ?>
<tr>
<td><?= strtoupper($row->MarketName); ?></td>

<td><?= $row->High; ?></td>
<td><?= $row->Low; ?></td>

<td><?= $row->TimeStamp; ?></td>

</tr>

<?php endforeach; endif; ?>


</tbody>
</table>


</div>
<!-- /widget-content --> 
</div>
<!-- /widget --> 
</div> <!-- /span8 -->
<div class="span4">
<!-- /widget -->
<p><a class="btn btn-success" href="<?= site_url('exchange/add_bot') ?>">Add Straggies</a></p>
<div class="widget widget-table action-table">

<?php if(isset($botdata) && $botdata != Null):   ?>
<?php foreach($botdata as $row): ?>
<div class="widget-header"> <i class="icon-th-list"></i>
<h3>BOT <?= strtoupper($row->name); ?></h3>
</div>
<!-- /widget-header -->
<div class="widget-content">


<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Market Name</th>
<th>Sell(@)</th>
<th>Buy(@)</th>
<th class="td-actions"> </th>

</tr>
</thead>
<tbody>

<tr>
<td><?= strtoupper($row->name); ?></td>

<td><?= $row->sell_limit; ?></td>
<td><?= $row->buy_limit; ?></td>

<td class="td-actions"><a href='<?= site_url("dashboard/bot/$row->id"); ?>' class="btn btn-small btn-success"><i class="btn-icon-only icon-ok"> </i></a><a href='#' class="btn btn-danger btn-small"><i class="btn-icon-only icon-remove"> </i></a>

</td>
</tr>

</tbody>
</table>

<!-- /widget-content --> 
</div>
<!-- /widget --> 
<p></p>
<?php endforeach; endif;?>

</div>



</div> <!-- /span8 -->
</div> <!-- /row -->

</div> <!-- /container -->
</div> <!-- /main-inner -->
</div> <!-- /main -->