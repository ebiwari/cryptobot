<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Login -BOT Management System</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes"> 
<link href="<?= base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url(); ?>public/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url(); ?>public/css/font-awesome.css" rel="stylesheet">
<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet"> -->
<link href="<?= base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>public/css/pages/signin.css" rel="stylesheet" type="text/css">
</head>

<body>

<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</a>
<a class="brand" href="<?= site_url('login'); ?>">BOT SYSTEM</a>
<div class="nav-collapse">
<ul class="nav pull-right">
<li class="">						
	<a href="<?= site_url('login/register'); ?>" class="">Don't have an account?	</a>	
</li>
<li class="">						
	<a href="#" class=""><i class="icon-chevron-left"></i>Back to Homepage</a>	
</li>
</ul>
</div><!--/.nav-collapse -->
</div> <!-- /container -->
</div> <!-- /navbar-inner -->
</div> <!-- /navbar -->



<div class="account-container">
<div class="content clearfix">
<?= form_open("login/login"); ?>

<h1>Member Login</h1>
<div class="login-fields">
<p>Please provide your details</p>

<div class="field">
	<label for="username">Username</label>
	<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
</div> <!-- /field -->

<div class="field">
	<label for="password">Password:</label>
	<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
</div> <!-- /password -->



</div> <!-- /login-fields -->

<div class="login-actions">
<span class="login-checkbox">
	<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
	<label class="choice" for="Field">Keep me signed in</label>
</span>
					
<button class="button btn btn-success btn-large" type="submit">Sign In</button>
</div> <!-- .actions -->

</form>
<h2 style="color:red;"><?= $this->session->userdata('message'); ?></h2>
</div> <!-- /content -->
</div> <!-- /account-container -->





<script src="<?= base_url(); ?>public/js/jquery-1.7.2.min.js"></script>
<script src="<?= base_url(); ?>public/js/bootstrap.js"></script>

<script src="<?= base_url(); ?>public/js/signin.js"></script>

</body>

</html>
