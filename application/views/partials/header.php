<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Dashboard - Commodo </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?= base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url(); ?>public/css/bootstrap-responsive.min.css" rel="stylesheet">
<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"  rel="stylesheet"> -->
<link href="<?= base_url(); ?>public/css/font-awesome.css" rel="stylesheet">
<link href="<?= base_url(); ?>public/css/style.css" rel="stylesheet">
<link href="<?= base_url(); ?>public/css/pages/dashboard.css" rel="stylesheet">
<link href="<?= base_url(); ?>public/select2/css/select2.min.css" rel="stylesheet">
<link href="<?= base_url(); ?>public/css/receipt.css" rel="stylesheet">
<link href="<?= base_url(); ?>public/css/print.css" media='print'>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php if(isset($data)){$vardata = $data;} ?>
</head>
<body>
<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container-fluid"> 
<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
              class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="<?= site_url('dashboard/index'); ?>">BOT SYSTEM </a>
<div class="nav-collapse">
  <ul class="nav pull-right">
    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i  class="icon-cog"></i> Account <b class="caret"></b></a>
      <ul class="dropdown-menu">
    
        <li><a href="<?= site_url('login/show'); ?>">Users</a></li>
      

        <li><a href="javascript:;">Settings</a></li>
        <li><a href="javascript:;">Help</a></li>
      </ul>
    </li>
    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="icon-user"></i> <?= strtoupper($this->session->userdata('first_name')); ?> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="javascript:;">Profile</a></li>
        <li><a href="<?= site_url('login/logout'); ?>">Logout</a></li>
      </ul>
    </li>
  </ul>
<form class="navbar-search pull-right" method="post" action="<?= site_url('cart/search'); ?>" autocomplete='off'>
  <input type="text" class="search-query" placeholder="Search" name="search">
</form>
</div>
<!--/.nav-collapse --> 
</div>
<!-- /container --> 
</div>
<!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
<div class="subnavbar-inner">
<div class="container" style="margin-left: 20px;">
<ul class="mainnav">

  <li class="<?php if(isset($page) && $page =='dashboard'){echo 'active';} ?>"><a href="<?= site_url('graph/show'); ?>"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>

  <li class="<?php if(isset($page) && $page =='strategies'){echo 'active';} ?>"><a href="<?= site_url('dashboard/britrex'); ?>"><i class="icon-beaker"></i><span>Strategies</span> </a></li>

   <li class="<?php if(isset($page) && $page =='trade'){echo 'active';} ?>"><a href="#"><i class=" icon-user-md"></i><span>Trade</span> </a></li>



  <li class="<?php if(isset($page) && $page =='funds'){echo 'active';} ?>"><a href="<?= site_url('dashboard/funds'); ?>"><i class="icon-credit-card"></i><span>Funds</span> </a> </li>



  <li class="<?php if(isset($page) && $page =='info'){echo 'active';} ?>"><a href="#"><i class="icon-credit-card"></i><span>Info</span> </a> </li>

   <li class="<?php if(isset($page) && $page =='exchange'){echo 'active';} ?>"><a href="<?= site_url('exchange/index'); ?>"><i class="icon-credit-card"></i><span>API Setting</span> </a> </li>




</ul>
</div>
<!-- /container --> 
</div>
<!-- /subnavbar-inner --> 
</div>
