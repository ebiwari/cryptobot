<!-- /extra -->
<div class="footer">
<div class="footer-inner">
<div class="container-fluid">
<div class="row-fluid">
<div class="span12"> &copy; <?= date('Y'); ?> <a href="#">Niger BOT</a>. </div>
  <!-- /span12 --> 
</div>
<!-- /row --> 
</div>
<!-- /container --> 
</div>
<!-- /footer-inner --> 
</div>
<!-- /footer --> 
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script type="text/javascript" src="<?=base_url(); ?>/public/js/jquery.chained.min.js"></script>



<script type="text/javascript">
  (function(){  

   $("#instruments").chained("#market");

    var dps = [];
    var chart = new CanvasJS.Chart("chartContainer", {
    

      title:{
        text: "Basic Candle Stick Chart"
      },
      zoomEnabled: true,
      axisY: {
        includeZero:false,
        title: "Prices",
         // minimum: 50
         // maximum:13000
        
      },
      axisX: {
        // interval:1,
        // intervalType: "month",
        // valueFormatString: "MMM-YY"
     
      },




      data: [{
        type: "candlestick",
        xValueType: "dateTime",
        // xValueFormatString: "MMM YYYY",
        yValueFormatString: "0.00000",
        dataPoints: dps
      }]
    });

      $.getJSON("<?= site_url('login/test'); ?>", parseData);

      // $.getJSON("https://canvasjs.com/data/gallery/php/qualcomm-stock-price.json", parseData);

      function parseData(result) {
      
    
        // for (var i = 0; i < result.length; i++) {
        //   dps.push({
        //     x: result[i].x,
        //     y: result[i].y
        //   });
        // }

        for (var i = 0; i < result.length; i++) {
          dps.push({

            x: result[i][0],
            // y: [result[i][1],result[i][2],result[i][3],result[i][4],result[i][5]]
            y: [result[i][1],result[i][2],result[i][3],result[i][4]]

          });          
        }

        console.log(dps);     

        chart.render();
   
      }
      



  }());

</script>





</body>
</html>
