<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">
<div class="span12"> 
<div class="widget ">
<div class="widget-header">
<i class="icon-user"></i>
<h3>Add Employee</h3>
</div> <!-- /widget-header -->

<div class="widget-content">



<?php  $attributes = array('class' => 'form-horizontal', 'id' => 'myform'); ?>
<?= form_open('login/register', $attributes); ?>
<fieldset>
<div class="control-group  <?php if(form_error('username')){echo 'warning'; } ?>">											
<label class="control-label" for="username">Username</label>
<div class="controls">
	<input type="text" class="span6" name="username" id="username" value="<?= set_select('username'); ?>">
	 <span class="help-inline"><?= form_error('username'); ?></span>
</div> <!-- /controls -->				
</div> <!-- /control-group -->

<div class="control-group  <?php if(form_error('password')){echo 'warning'; } ?>">											
<label class="control-label" for="first_name">Password</label>
<div class="controls">
	<input type="password" class="span6" name="password" id="password" value="">
	
</div> <!-- /controls -->				
</div> <!-- /control-group -->


<div class="control-group  <?php if(form_error('password')){echo 'warning'; } ?>">											
<label class="control-label" for="first_name">Confirm Password</label>
<div class="controls">
	<input type="password" class="span6" name="confirm_password" id="confirm_password" value="">
	 <span class="help-inline"><?= form_error('confirm_password'); ?></span>
</div> <!-- /controls -->				
</div> <!-- /control-group -->


<br /><br />

<div class="control-group  <?php if(form_error('first_name')){echo 'warning'; } ?>">											
<label class="control-label" for="first_name">First Name</label>
<div class="controls">
	<input type="text" class="span6" name="first_name" id="first_name" value="<?= set_value('first_name'); ?>">
</div> <!-- /controls -->				
</div> <!-- /control-group -->


<div class="control-group <?php if(form_error('last_name')){echo 'warning'; } ?>">											
<label class="control-label" for="last_name">Last Name</label>
<div class="controls">
	<input type="text" class="span6" name = 'last_name' id="last_name"  value="<?= set_value('last_name'); ?>">
</div> <!-- /controls -->				
</div> <!-- /control-group -->



<div class="control-group">                     
<label class="control-label" for="other_name">Other Name</label>
<div class="controls">
  <input type="text" class="span6" name = 'other_name' id="other_name" value="">
</div> <!-- /controls -->       
</div> <!-- /control-group -->



<div class="control-group <?php if(form_error('first_name')){echo 'warning'; } ?>">											
<label class="control-label" for="email">Email Address</label>
<div class="controls">
	<input type="text" class="span4" name="email" id="email"  value="">
  
</div> <!-- /controls -->				
</div> <!-- /control-group -->



<div class="control-group  <?php if(form_error('phone')){echo 'warning'; } ?>">                     
<label class="control-label" for="phone">Phone</label>
<div class="controls">
  <input type="text" class="span4" name="phone" id="phone"  value="<?= set_select('phone'); ?>">
</div> <!-- /controls -->       
</div> <!-- /control-group -->


<br /><br />





<div class="control-group <?php if(form_error('doctor')){echo 'warning'; } ?>">                     
<label class="control-label" for="doctor">Group</label>
<div class="controls">
<select name="group_id"  id="group_id" class="span5 selectpicker" >

<?php if(isset($groups) && $groups != Null): ?>
<option value=""></option>
<?php foreach($groups as $row): ?>
<option value="<?= $row->id; ?>"><?= strtoupper($row->name); ?></option>
<?php endforeach; ?>
<?php endif; ?>


</select>


 
</div> <!-- /controls -->       
</div> <!-- /control-group -->



<br />


<div class="form-actions">
<button type="submit" class="btn btn-primary">Save</button> 
<button class="btn">Cancel</button>
</div> <!-- /form-actions -->
</fieldset>
</form>
</div>








</div> <!-- /widget-content -->
</div> <!-- /widget -->
</div> <!-- /span8 -->
</div> <!-- /row -->
</div> <!-- /container -->
</div> <!-- /main-inner -->
</div> <!-- /main -->