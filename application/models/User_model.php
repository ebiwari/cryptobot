<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model { 

    public function __construct(){
        parent::__construct();
        
    } 

    public function login($username,$password){
        $this->db->select('*')->from('users');
        $this->db->where(array('username'=>$username,'password'=>$password)); 
        $result = $this->db->get();
        if($result->num_rows() > 0){
            $row = $result->row();

          
            $this->db->select('members.id as member_id,users.username,members.first_name,members.last_name')->from('users');
            $this->db->join('members','users.username=members.user_id');            
            $this->db->where('users.username',$username);
            $results = $this->db->get();

            if($results->num_rows() > 0){
                $row = $results->row();
                $this->session->set_userdata(array(
                     'member_id'=>$row->member_id,
                     'username'=>$row->username,                  
                     'first_name'=>$row->first_name,
                     'last_name'=>$row->last_name
            ));
                return True;
            }
            return False;
        }

        return False;
    }




    public function get(){
        $this->db->select('*')->from('users');
        $result = $this->db->get();
        if($result->num_rows() > 0){
            return $result->result();
        }

        return Null;

    }


    public function get_group(){
        $this->db->select('id,name')->from('groups');
        $result = $this->db->get();
        if($result->num_rows() > 0){
            return $result->result();
        }

        return Null;

    }

    public function check_username($username){
        $this->db->select('id')->from('users');
        $this->db->where(array('username'=>$username));

        $result = $this->db->get();
        if($result->num_rows() > 0){
            return True;
        }

        return False;

    }


    public function register($password){
        $this->db->insert('users',array(
            'username'=>$this->input->post('username'),
            'password'=>$password
        ));

        $this->db->insert('members',array(
            'user_id'=>$this->input->post('username'),
            'email'=>$this->input->post('email'),

            'first_name'=>$this->input->post('first_name'),
            'last_name'=>$this->input->post('last_name'),        
            'phone'=>$this->input->post('phone')

        ));


      

    }












 

 







}