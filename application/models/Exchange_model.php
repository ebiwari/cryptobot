<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Exchange_model extends CI_Model { 

    public function __construct(){
        parent::__construct();
        
    }   


    public function get(){
        $this->db->select('*')->from('exchange');
        $this->db->where('member_id',$this->session->userdata('member_id'));
        $result = $this->db->get();

        if($result->num_rows() > 0){
            return $result->row();
        }

        return Null;

    }



    public function get_bot(){
        $this->db->select('*')->from('bots');
        $this->db->where('member_id',$this->session->userdata('member_id'));
        $result = $this->db->get();

        if($result->num_rows() > 0){
            return $result->result();
        }

        return Null;

    }



    public function save(){      

        $this->db->insert('exchange', array(
            'name'=>$this->input->post('name'),
            'apikey'=>$this->input->post('apikey'),
            'apisecret'=>$this->input->post('apisecret'),
            'member_id'=>$this->session->userdata('member_id')
     
          
        ));


    }




    public function save_bot(){   
        $this->db->insert('bots', array(
            'name'=>$this->input->post('name'),
            'sell_limit'=>$this->input->post('sell_limit'),
            'buy_limit'=>$this->input->post('buy_limit'),
            'member_id'=>$this->session->userdata('member_id') 
          
        ));


    }


    










 

 







}