-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 29, 2018 at 11:41 AM
-- Server version: 5.5.59-0ubuntu0.14.04.1
-- PHP Version: 5.6.33-3+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `botexchange`
--

-- --------------------------------------------------------

--
-- Table structure for table `bots`
--

CREATE TABLE IF NOT EXISTS `bots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `buy_limit` int(4) NOT NULL,
  `sell_limit` int(4) NOT NULL,
  `market_name` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `bots`
--

INSERT INTO `bots` (`id`, `member_id`, `name`, `buy_limit`, `sell_limit`, `market_name`, `created_at`, `update_at`) VALUES
(2, 1, 'Mercury', 10, 10, '', '2018-04-29 16:19:03', '0000-00-00 00:00:00'),
(3, 1, 'Venus', 20, 20, '', '2018-04-29 16:42:40', '0000-00-00 00:00:00'),
(4, 1, 'Saturn', 10, 10, '', '2018-05-01 10:22:13', '0000-00-00 00:00:00'),
(5, 1, 'pluto', 10, 10, '', '2018-05-09 16:37:31', '0000-00-00 00:00:00'),
(6, 1, 'venus', 20, 20, '', '2018-05-09 16:39:49', '0000-00-00 00:00:00'),
(7, 2, 'Saturn', 3, 4, '', '2018-05-10 16:09:20', '0000-00-00 00:00:00'),
(8, 3, 'Saturn', 10, 10, '', '2018-05-10 16:24:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE IF NOT EXISTS `exchange` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `apikey` varchar(100) DEFAULT NULL,
  `apisecret` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Exchange_member_idx` (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exchange`
--

INSERT INTO `exchange` (`id`, `name`, `apikey`, `apisecret`, `password`, `created_at`, `updated_at`, `member_id`) VALUES
(3, 'bittex', 'b6abcfba67b344658eb6e151ee14a859', 'a1e64558202e46848c9dc2121a8a8b46', NULL, '2018-04-24 15:55:48', NULL, 1),
(4, 'bittex', 'b6abcfba67b344658eb6e151ee14a859', 'a1e64558202e46848c9dc2121a8a8b46', NULL, '2018-05-10 16:10:13', NULL, 2),
(5, 'poliniex', 'b6abcfba67b344658eb6e151ee14a859', 'a1e64558202e46848c9dc2121a8a8b46', NULL, '2018-05-10 16:25:54', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `other_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `user_id`, `first_name`, `last_name`, `other_name`, `email`, `phone`, `dob`, `address`, `created_at`, `updated_at`) VALUES
(1, 'ebiwari', 'ebiwari', 'williams', NULL, 'ebiwari@gmail.com', '07039564613', '2018-04-04', '11 gunning road ai', '2018-04-24 15:48:45', NULL),
(2, 'josh', 'Joshua', 'williams', NULL, NULL, '07039564613', NULL, NULL, '2018-05-10 16:03:37', NULL),
(3, 'rose', 'rose', 'williams', NULL, NULL, NULL, NULL, NULL, '2018-05-10 16:24:05', NULL),
(4, 'douebi', 'Douebi', 'Williams', NULL, 'demo@kryll.io', '07039564644', NULL, NULL, '2018-05-17 16:59:24', NULL),
(5, 'comfort', 'Comfort', 'Taavan', NULL, 'comfort@gmail.com', '7039564688', NULL, NULL, '2018-05-17 17:24:29', NULL),
(6, 'giltexo', 'Gilbert', 'Ifeanyichukwu', NULL, 'giltexo@yahoo.com', '2347061213433', NULL, NULL, '2018-05-18 16:33:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payments_member1_idx` (`member_id`),
  KEY `fk_payments_subscription1_idx` (`subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `amount` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'ebiwari', '68f291c3e958aa8013d42e67a807df13', '2018-04-24 15:47:53', NULL),
(2, 'josh', '68f291c3e958aa8013d42e67a807df13', '2018-05-10 16:01:51', NULL),
(3, 'rose', '68f291c3e958aa8013d42e67a807df13', '2018-05-10 16:23:31', NULL),
(4, 'douebi', '68f291c3e958aa8013d42e67a807df13', '2018-05-17 16:59:24', NULL),
(5, 'comfort', '68f291c3e958aa8013d42e67a807df13', '2018-05-17 17:24:29', NULL),
(6, 'giltexo', '4ae7e6c6a479587aecd90dc353205432', '2018-05-18 16:33:51', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `exchange`
--
ALTER TABLE `exchange`
  ADD CONSTRAINT `fk_Exchange_member` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `fk_payments_member1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payments_subscription1` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;